﻿using System;

namespace CoalescingOp
{
    class Person
    {
        private string name;
        public string Name
        {
            get => name;
            set => name =
                value ??
                throw new ArgumentNullException(
                    nameof(value),
                    $"{nameof(Name)} cannot be null");
        }
    }

    public class CoalescingDemo
    {
        static bool doSomething(int a, int b)
        {
            return a > b;
        }
        /// <summary>
        /// Demo here various effects:
        /// with assigning a name, not assigning a name
        /// using nullable type check in condition
        /// </summary>
        public static void Main()
        {

            int b;
            int? a; //= null;
            a = null;
            //a = 7;
            if (a != null)
            {
                Console.WriteLine(a.Value);
            }
            else
            {
                Console.WriteLine("a == null");
            }


            Person p = null;// = new Person();
            //p.Name = null; // = "Jack";

            // so far
            string name; //= (p.Name != null) ? p.Name : "Unknown";

            // using null coalescing operator
            name = p?.Name ?? "Unknown";

            Console.WriteLine($"Name: {name}");
            p = new Person();
            p.Name = null;

            Console.ReadKey();
        }

        private string name;
        public string Name
        {
            get => name;
            set => name =
                value ??
                throw new ArgumentNullException(
                    nameof(value),
                    $"{nameof(Name)} cannot be null");
        }

        private static void DemoNullable()
        {
            int? a = 42;
            if (a is int valueOfA)
            {
                Console.WriteLine($"a is {valueOfA}");
            }
            else
            {
                Console.WriteLine("a does not have a value");
            }
        }

    }
}
