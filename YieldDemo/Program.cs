﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace YieldDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("With List\n=========");
            foreach (var item in GetListWithElementElementsMax(10))
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();


            Console.WriteLine("\nWith yield\n==========");
            foreach (var item in YieldWithElementsMax(10))
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();


            Console.WriteLine("\nWith Enummerable\n================");
            foreach (var item in new ZeroToMaxEnummerable(10))
            {
                Console.WriteLine(item);
            }

            Console.ReadLine();
        }

        // the "classic" way.
        public static IEnumerable<int> GetListWithElementElementsMax(int num)
        {
            List<int> list = new List<int>();
            for (var i = 0; i < num; i++)
            {
                list.Add(i);
            }

            return list;
        }

        // the yield way
        public static IEnumerable<int> YieldWithElementsMax(int num)
        {
            for (var i = 0; i < num; i++)
                yield return i;
        }

        // the selfmade iterator way
        public class ZeroToMaxEnummerable : IEnumerable<int>
        {
            // Iterator for this enummerable
            public class ZeroToMaxIterator : IEnumerator<int>
            {
                private readonly int _max;

                public ZeroToMaxIterator(int max)
                {
                    _max = max;
                }

                public void Dispose()
                {
                }

                public bool MoveNext()
                {
                    if (this.Current < this._max - 1)
                    {
                        this.Current++;
                        return true;
                    }

                    return false;
                }

                public void Reset()
                {
                    this.Current = -1;
                }

                public int Current { get; private set; } = -1;

                object IEnumerator.Current
                {
                    get { return Current; }
                }
            }


            private int _max;

            public ZeroToMaxEnummerable(int max)
            {
                _max = max;
            }

            public IEnumerator<int> GetEnumerator()
            {
                return new ZeroToMaxIterator(this._max);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }
    }
}
