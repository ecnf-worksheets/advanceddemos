﻿using System;

namespace ExtensionDemo
{
    public static class StringExtensionsOldStyle
    {
        public static string Without(string text, char ch)
        {
            return string.Join("", text.Split(ch));
        }
    }

    public static class StringExtensions
    {
        public static String Without(this string text, char ch)
        {
            return string.Join("", text.Split(ch));
        }
    }

    public class ExtensionDemo
    {
        public static void Main()
        {
            var text = "Hxellxo";
            Console.WriteLine(StringExtensionsOldStyle.Without(text, 'x'));

            Console.WriteLine(text.Without('x'));
            Console.WriteLine("Hxellxo".Without('x'));

            Console.ReadKey();

        }
    }
}
