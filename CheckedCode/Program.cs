﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckedCode
{
    class Program
    {
        static void Main(string[] args)
        {
            //Demo1Nullable();
            DemoCheckedBlocks();
            Console.ReadLine();
        }

        private static void DemoCheckedBlocks()
        {
            //checked
            {
                int ten = 10;
                int i = Int32.MaxValue + ten;

                //would print -2147483639    
                Console.WriteLine(i);
            }

        }

        private static void Demo1Nullable()
        {
            // the short form for Nullable<int> b = null;
            int? b = null;

            b = 10;

            // you could also use b != null
            // the compiler converts it to HasValue
            if (b.HasValue)
            {
                Console.WriteLine(b);
            }
            else
            {
                Console.WriteLine("b is null");
            }

            Console.WriteLine($"b: {b ?? 0}");

        }

        private void Demo2Nullable()
        {
            var pers = new Person();

            // using the null coalescing operator
            string name = pers?.Name ?? "Unknown";

        }

    }

    class Person
    {
        public string Name { get; set; }
    }
}
