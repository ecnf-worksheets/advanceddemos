﻿using System;

namespace RationalApp
{
    class Program
    {
        static void Main()
        {
            Rational r1 = new Rational(3, 2);
            Rational r2 = new Rational(3, 2);

            r1 = r1*2;
            r1 = 2*r1;

            r1 = 2;

            string s = r1;

            double d = (double) r1;

            Console.WriteLine($"Equals {r1.Equals(r1)}");
            Console.WriteLine($"== {r1 == r1}");

            Console.ReadKey();
        }
    }
}
