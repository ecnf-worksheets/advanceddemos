using System;

namespace RationalApp
{
    public struct Rational
    {
        public Rational(int nominator, int denominator)
        {
            this.Nominator = nominator;
            this.Denominator = denominator;
        }

        public static implicit operator Rational(int i)
        {
            return new Rational(i, 1);
        }

        public static explicit operator double (Rational r)
        {
            return r.Nominator / (double)r.Denominator;
        }

        public static implicit operator string(Rational r)
        {
            return r.ToString();
        }

        public override string ToString()
        {
            return $"{Nominator}/{Denominator}";
        }

        public int Nominator { get; set; }
        public int Denominator { get; set; }

        public static Rational operator *(Rational lhs, int i)
        {
            return new Rational(lhs.Nominator*i, lhs.Denominator);
        }

        public static Rational operator *(int i, Rational rhs)
        {
            return new Rational(rhs.Nominator * i, rhs.Denominator);
        }
        public static Rational operator +(int i, Rational rhs)
        {
            return new Rational(rhs.Nominator + i, rhs.Denominator);
        }
    }
}